package br.com.massuda.alexander.autenticacao.api.bo;

import java.util.List;

import br.com.massuda.alexander.spring.framework.infra.excecoes.Erro;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

public interface BOUsuario {

	public Usuario autentica(String login, String senha) throws Erro;

	public List<Usuario> pesquisarPorNome(String nome);

	public Usuario pesquisarPorId(String id);
	
}
