/**
 * 
 */
package br.com.massuda.alexander.autenticacao.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.massuda.alexander.autenticacao.api.bo.BOUsuario;
import br.com.massuda.alexander.spring.framework.infra.excecoes.Erro;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

/**
 * @author Alex
 *
 */
@CrossOrigin
@Controller
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private BOUsuario bo;
	
	@ResponseBody
	@PostMapping("/autenticar")
	public Usuario autenticar (@RequestBody Usuario usuario) throws Erro {
		return bo.autentica(usuario.getLogin(), usuario.getSenha());
	}
	

	@ResponseBody
	@GetMapping("/id/{id}")
	public Usuario pesquisarPorId (@PathVariable("id") Long id) {
		return bo.pesquisarPorId(String.valueOf(id));
	}
	
	
	@ResponseBody
	@GetMapping
	public List<Usuario> pesquisarPorNome (@RequestParam String nome) {
		return bo.pesquisarPorNome(nome);
	}
	
	
}
