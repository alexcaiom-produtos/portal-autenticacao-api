package br.com.massuda.alexander.autenticacao.api.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientException;

import br.com.massuda.alexander.spring.framework.infra.excecoes.Erro;
import br.com.massuda.alexander.spring.framework.infra.excecoes.Mensagem;
import br.com.waiso.framework.abstratas.Classe;

@ControllerAdvice
public class TratadorDeExcecoes {
	
	private final Logger LOGGER = LogManager.getLogger(getClass());

	@ExceptionHandler({RestClientException.class})
	public ResponseEntity<Erro> tratarErroHTTP(RestClientException e) {
		e.printStackTrace();
		Erro erro;
		if (Classe.existe(e.getCause())) {
			erro = (Erro) e.getCause();
		} else {
			erro = new Erro(e, e.getMessage());
		}
		return new ResponseEntity<Erro>(erro, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler({Erro.class})
	public ResponseEntity<Mensagem> tratarErroHTTP(Erro e) {
//		LOGGER.error("Erro:", e);
		Mensagem mensagem = new Mensagem(e, e.getMensagem());
		return new ResponseEntity<Mensagem>(mensagem, HttpStatus.BAD_REQUEST);
	}
	
}
