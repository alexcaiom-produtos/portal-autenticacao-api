/**
 * 
 */
package br.com.massuda.alexander.autenticacao.api.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.massuda.alexander.autenticacao.api.service.ServicoUsuario;
import br.com.massuda.alexander.spring.framework.infra.excecoes.Erro;
import br.com.massuda.alexander.spring.framework.infra.excecoes.ErroNegocio;
import br.com.massuda.alexander.spring.framework.infra.utils.UtilsData;
import br.com.massuda.alexander.usuario.orm.bo.BO;
import br.com.massuda.alexander.usuario.orm.modelo.NivelHierarquico;
import br.com.massuda.alexander.usuario.orm.modelo.RespostaUsuarioAutenticacao;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

/**
 * @author Alex
 *
 */
@Component
public class BOUsuarioImpl  extends BO
							implements BOUsuario {

	@Autowired
	private ServicoUsuario servico;
	
	/**
	 * Metodo de Autenticacao de Usuario
	 * @param login
	 * @param senha
	 * @return
	 */
	public Usuario autentica(String login, String senha) throws Erro {
		log("Autenticando "+getNomeEntidade());
		Usuario u = new Usuario();
		u.setLogin(login);
		Usuario usuarioRecuperadoDoBD = new  Usuario();
		usuarioRecuperadoDoBD = servico.pesquisarPorLogin(u.getLogin());
		
		//Primeiro verificamos se o usuario existe.
		if(existe(usuarioRecuperadoDoBD)){
			//Verificamos se usuario e senha informado batem com o banco.
			boolean usuarioConfere = (login.equals(usuarioRecuperadoDoBD.getLogin()) && senha.equals(usuarioRecuperadoDoBD.getSenha()));
			boolean usuarioEstaBloqueado = !(usuarioRecuperadoDoBD.getStatus() == null || !usuarioRecuperadoDoBD.getStatus().equals(RespostaUsuarioAutenticacao.USUARIO_BLOQUEADO));
			boolean ehFinalDeSemana = UtilsData.ehFinalDeSemana();
			boolean ehHorarioComercial = UtilsData.ehHorarioComercial();
			boolean ehAdm = ehAdm(usuarioRecuperadoDoBD);
			
			boolean usuarioPodeAcessar = usuarioConfere 
								&& 
								((!usuarioEstaBloqueado 
								/*&& !ehFinalDeSemana*/ 
								/*&& ehHorarioComercial*/)
								|| ehAdm); 
			
			if (usuarioPodeAcessar){
				log(getNomeEntidade()+ " autenticado com sucesso");
			} else if (!usuarioConfere) {
				log(getNomeEntidade()+ " informado com senha inválida...");
				usuarioRecuperadoDoBD.setContadorSenhaInvalida(usuarioRecuperadoDoBD.getContadorSenhaInvalida()+1);
				boolean avisaUsuarioBloqueioNoProximoErro = (3 - usuarioRecuperadoDoBD.getContadorSenhaInvalida()) == 1;
				trataSenhaInvalidaDoUsuario(usuarioRecuperadoDoBD);
				
				if (avisaUsuarioBloqueioNoProximoErro) {
					throw new ErroNegocio(RespostaUsuarioAutenticacao.SENHA_INVALIDA_ULTIMA_TENTATIVA.getMensagem());
				} else {
					throw new ErroNegocio(RespostaUsuarioAutenticacao.SENHA_INVALIDA.getMensagem());
				}
			} else if(usuarioEstaBloqueado) {
				throw new ErroNegocio(RespostaUsuarioAutenticacao.USUARIO_BLOQUEADO.getMensagem() );
			} else if (ehFinalDeSemana) {
				throw new ErroNegocio(RespostaUsuarioAutenticacao.FINAL_DE_SEMANA.getMensagem() );
			} else if (!ehHorarioComercial) {
				throw new ErroNegocio(RespostaUsuarioAutenticacao.FORA_DO_HORARIO_COMERCIAL.getMensagem() );
			}
		} else {
			log(RespostaUsuarioAutenticacao.USUARIO_INEXISTENTE.getMensagem());
			throw new ErroNegocio(RespostaUsuarioAutenticacao.USUARIO_INEXISTENTE.getMensagem());
		}
		return usuarioRecuperadoDoBD;
	}

	
	private boolean ehAdm(Usuario usuarioRecuperadoDoBD) {
		return usuarioRecuperadoDoBD.getPerfil().getNivel() == NivelHierarquico.OPERADOR;
	}

	private void trataSenhaInvalidaDoUsuario(Usuario usuarioRecuperadoDoBD) {
		int numeroDeChancesRestantes = 3 - usuarioRecuperadoDoBD.getContadorSenhaInvalida();
		boolean deveBloquearUsuario = numeroDeChancesRestantes == 0;
		if (deveBloquearUsuario) {
			usuarioRecuperadoDoBD.setStatus(RespostaUsuarioAutenticacao.USUARIO_BLOQUEADO);				
		}
		servico.editar(usuarioRecuperadoDoBD);
	}


	public List<Usuario> pesquisarPorNome(String nome) {
		return servico.pesquisarPorNome(nome);
	}


	public Usuario pesquisarPorId(String id) {
		return servico.pesquisarPorId(id);
	}
	
	
}
