package br.com.massuda.alexander.autenticacao.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortalAutenticacaoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortalAutenticacaoApiApplication.class, args);
	}
}
