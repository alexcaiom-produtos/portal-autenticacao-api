package br.com.massuda.alexander.autenticacao.api.service;

import java.util.List;

import javax.annotation.PostConstruct;
//import javax.ws.rs.client.Entity;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.spring.framework.infra.servico.Servico;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

@Service
public class ServicoUsuario extends Servico<Usuario> {

	@Value("${api.usuario.servidor}")
	private String servicoUsuarioServidor;
	@Value("${api.usuario.porta}")
	private String servicoUsuarioPorta;
	@Value("${api.usuario.servico}")
	private String servicoUsuarioContexto;
	private String url;

	@PostConstruct
	public void postInit() {
		url = new StringBuilder()
				.append("http://").append(servicoUsuarioServidor).append(":").append(servicoUsuarioPorta)
				.append(servicoUsuarioContexto).append("/").append(entidade).toString();
//		target = client.target(url);
		tipo = Usuario.class;
	}

	public void editar(Usuario usuarioRecuperadoDoBD) {
		restConector.postForEntity(url, usuarioRecuperadoDoBD);
//		target.request()
//		.post(Entity.entity(usuarioRecuperadoDoBD, MediaType.APPLICATION_JSON_TYPE));
		
	}

	public Usuario pesquisarPorLogin(String login) {
		StringBuilder urlTemp = new StringBuilder(url.toString());
		urlTemp.append("/login/").append(login);
		Usuario usuario = restConector.getForObject(urlTemp.toString());
//		Response resposta = target
//				.path("pesquisarPorLogin")
//				.queryParam("login", login)
//				.request().get();
//		Usuario usuario = getEntity(resposta);
		return usuario;
	}

	public List<Usuario> pesquisarPorNome(String nome) {
//		Response resposta = target
//				.path("pesquisarPorNome")
//				.queryParam("nome", nome)
//				.request().get();
//		List<Usuario> usuario = getEntityList(resposta);
		List<Usuario> usuarios = (List<Usuario>) restConector.getForObject(url+"/nome/"+nome, Usuario[].class);
		return usuarios;
	}

	public Usuario pesquisarPorId(String id) {
//		Response resposta = target
//				.path("id")
//				.path(id)
//				.request().get();
//		return getEntity(resposta);
		return restConector.getForEntity(url+"/id/"+id);
	}
}
